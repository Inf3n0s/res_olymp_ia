
# XGBoost methods
https://www.kaggle.com/code/stuarthallows/using-xgboost-with-scikit-learn

# Metrics and scoring
https://scikit-learn.org/stable/modules/model_evaluation.html

# classification report
https://scikit-learn.org/stable/modules/generated/sklearn.metrics.classification_report.html

# metrics
https://scikit-learn.org/stable/modules/generated/sklearn.metrics.mean_absolute_error.html

# confusion matrix
https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html

# metric for eval
https://www.analyticsvidhya.com/blog/2021/07/metrics-to-evaluate-your-classification-model-to-take-the-right-decisions/



# data visualization matplotlib
https://www.kaggle.com/code/sanikamal/data-visualization-using-matplotlib



<br>
<br>
<br>
<br>
<br>


Pentru regresie:
MAE
MSE
RMSE
Pentru clasificare generala:
Accuracy
Pentru clasificare binara (adica doar 2 clase):

Precission = TruePositives / (TruePositives + FalsePositives) - practic cate din cele pe care tu le-ai prezis ca fiind 1, chiar sunt 1




Recall = TruePositives / (TruePositives + FalseNegatives) - practic cate din cele pozitive (anotate cu 1) au fost detectate de tine
F1Score = (2 * Precission * Recall) / (Precission + Recall)
