{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, ... }@inputs:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          config.allowUnfree = true;
          config.cudaSupport = true;
        };

        torchsampler = pkgs.python3Packages.callPackage ./nix/torchsampler.nix {};

        python = pkgs.python3.withPackages
          (pp: with pp; [
            # Python + Jupyter
            jupyter
            jupyterlab
            ipython
            tqdm

            # Data manipulation and visualisation
            numpy
            pandas
            matplotlib
            seaborn

            # ML
            statsmodels
            torch
            torchvision
            torchsampler
            pytorch-lightning
            scikit-learn

            # NLP
            nltk
            gensim
            num2words
            emoji

            # Utils
            wikipedia
          ]);

        packages = with pkgs; [
          pyright
          python
        ];

      in {
        packages = {
          inherit torchsampler;
        };

        devShells.default = pkgs.mkShell {
          inherit packages;
        };
      }
    );
}
