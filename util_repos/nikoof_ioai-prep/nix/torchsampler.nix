{
  buildPythonPackage,
  fetchPypi,
  numpy,
  pandas,
  torch,
  torchvision,
}:

let
  pname = "torchsampler";
  version = "0.1.2";
  format = "wheel";
in
buildPythonPackage {
  inherit pname version format;

  src = fetchPypi {
    inherit pname version format;
    python = "py3";
    dist = "py3";
    hash = "sha256-p3odS7fV8TSzHKSv9N6WR9mqD0FqYuMVpAPyU+uvJE8=";
  };

  propagatedBuildInputs = [
    numpy
    pandas
    torch
    torchvision
  ];

  doCheck = false;

  pythonImportsCheck = [ "torchsampler" ];
}
