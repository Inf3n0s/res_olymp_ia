# Personal resources for IOAI

# Machine learning
## Courses
- [Stanford CS229](https://www.youtube.com/playlist?list=PLoROMvodv4rMiGQp3WXShtMGgzqpfVfbU) (note lectures 1-5)
- ~~[Nitro NLP 2024](https://www.youtube.com/playlist?list=PLByLNt67Fbif2v8OyERD8QKQgrfHSwhJK)~~
- ~~[3Blue1Brown Neural Networks](https://youtube.com/playlist?list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi&si=hJoTKG1GeariR-fA)~~

## Books
- [Deep Learning - Ian Goodfellow, Yoshua Bengio, Aaron Courville](https://www.deeplearningbook.org/) (direct chineză)
- [Neural Networks and Deep Learning - Michael Nielsen](http://neuralnetworksanddeeplearning.com/index.html) (mai umană)
- [Mathematics for Machine Learning - M. P. Deisenroth, A. A Faisal, Cheng Soon Ong](https://mml-book.github.io/book/mml-book.pdf)

## Tutorials
- [UvA Deep Learning Tutorials](https://uvadlc-notebooks.readthedocs.io/en/latest/index.html) (⭐⭐⭐⭐⭐)

## Demos
- [minGPT](https://github.com/karpathy/minGPT)

---

# Natural language processing
## Books
- [Speech and Language Processing - Dan Jurafsky, James Martin](https://web.stanford.edu/~jurafsky/slp3/)

## Light
- [How to solve 90% of NLP problems](https://blog.insightdatascience.com/how-to-solve-90-of-nlp-problems-a-step-by-step-guide-fda605278e4e)
- [Illustrated word2vec](https://jalammar.github.io/illustrated-word2vec/)
- [Implementing word2vec from scratch](https://towardsdatascience.com/implementing-word2vec-in-pytorch-from-the-ground-up-c7fe5bf99889)

---

# Computer vision
## Courses
- [Stanford CS231n](https://www.youtube.com/playlist?list=PL3FW7Lu3i5JvHM8ljYj-zLfQRF3EO8sYv)

## Books
- [Computer Vision: Algorithms and Applications - Richard Szeliski](https://szeliski.org/Book/)

---

# Transformers
## Courses
- [Nitro NLP Transfer Learning & Transformers](https://www.youtube.com/watch?v=YtvlVnCBRO0)

## Light
- [The Illustrated Transformer](https://jalammar.github.io/illustrated-transformer/)
- [LLM Visualisation](https://bbycroft.net/llm)
