-- mate-utila --
https://www.youtube.com/watch?v=fNk_zzaMoSs&list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab
https://www.youtube.com/watch?v=WUvTyaaNkzM&list=PLZHQObOWTQDMsr9K-rj53DwVRMYO3t5Yr
https://calculusmadeeasy.org/
https://mml-book.github.io/book/mml-book.pdf

-- python --
https://colab.research.google.com/drive/1MFJSiUNrm83P2oM4JdVjsI7X198uApM5?usp=sharing (Intro Colab)
https://colab.research.google.com/drive/13HW52Wset7S2Qg0keKfazTQUvQcF7vOQ?usp=sharing (Prea mult Python)
https://www.pyml.ro/
https://colab.research.google.com/github/Nyandwi/machine_learning_complete/blob/main/0_python_for_ml/intro_to_python.ipynb  (cu extra resurse la final) de la ioai-official
Acest tutorial de [PyTorch](https://youtu.be/Z_ikDlimN6A?si=Pk-UDRIvmgJY4_FZ) este bine structurat și conține multe elemente ce vă pot fi de folos, fiecare are timestamp-uri pentru a căuta rapid ce aveți nevoie.


-- machine learning -- 
- Nitro NLP [Machine Learning](https://youtu.be/V3cfWPzfkqo?si=1WEQ2Ok80XN9wVLH)
- Lecțiile pyML, [partea II](https://www.pyml.ro/machine-learning-lectii-online-in-python.php)
- [Stanford CS229, lectures 1-5](https://www.youtube.com/watch?v=jGwO_UgTS7I&list=PLoROMvodv4rMiGQp3WXShtMGgzqpfVfbU&ab_channel=StanfordOnline)


-- deep learning --
https://www.deeplearningbook.org/
https://www.kaggle.com/learn
- [Deep learning cu exemple, cod, explicatii](http://neuralnetworksanddeeplearning.com/index.html)
- [Deep learning 3Blue1Brown](https://www.youtube.com/watch?v=aircAruvnKk&list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi)
Avansat: 

 Pe canalul EEML (www.eeml.eu) https://www.youtube.com/@eemlcommunity3531/playlists lectures din 2 ani precedenti la scoala de vara. Cel putin unele cursuri s-ar putea sa fie foarte relevante (e.g. Intro to DL, dar si Theory of DL, Attention and Transformers, etc.)
 
** https://karpathy.github.io/neuralnets/ ** (reguli pentru DL)
Tutorial de getting started de la PyTorch: https://pytorch.org/tutorials/beginner/deep_learning_60min_blitz.html

-- natural-language-processing -- 
https://blog.insightdatascience.com/how-to-solve-90-of-nlp-problems-a-step-by-step-guide-fda605278e4e
https://jalammar.github.io/illustrated-word2vec/
https://web.stanford.edu/~jurafsky/slp3/
https://gitlab.com/nitronlp/Workshops/-/tree/main (workshopuri nitronlp)

-- computer-vision --
https://szeliski.org/Book/
https://www.youtube.com/watch?v=vT1JzLTH4G4

-- transformers -- 
https://www.youtube.com/watch?v=YtvlVnCBRO0
https://jalammar.github.io/illustrated-transformer/
https://bbycroft.net/llm

-- extra --
https://www.kaggle.com/datasets/mlg-ulb/creditcardfraud/discussion/277570
https://www.kaggle.com/code/hasibur013/house-prices-prediction-competition
https://www.kaggle.com/code/jamescfabrianes/house-prices-advanced-regression-techniques

